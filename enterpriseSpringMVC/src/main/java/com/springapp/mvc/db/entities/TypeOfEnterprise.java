package com.springapp.mvc.db.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class TypeOfEnterprise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nameOfTypeEnterprise;

    @OneToMany(mappedBy = "typeOfEnterprise")
    private List<Enterprise> enterprises;

    public TypeOfEnterprise() {
    }

    public TypeOfEnterprise(long id, String nameOfTypeEnterprise) {
        this.id = id;
        this.nameOfTypeEnterprise = nameOfTypeEnterprise;
    }

    public TypeOfEnterprise(String nameOfTypeEnterprise) {
        this.id = 0;
        this.nameOfTypeEnterprise = nameOfTypeEnterprise;
    }




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameOfTypeEnterprise() {
        return nameOfTypeEnterprise;
    }

    public void setNameOfTypeEnterprise(String nameOfTypeEnterprise) {
        this.nameOfTypeEnterprise = nameOfTypeEnterprise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeOfEnterprise that = (TypeOfEnterprise) o;

        if (id != that.id) return false;
        if (nameOfTypeEnterprise != null ? !nameOfTypeEnterprise.equals(that.nameOfTypeEnterprise) : that.nameOfTypeEnterprise != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nameOfTypeEnterprise != null ? nameOfTypeEnterprise.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TypeOfEnterprise{" +
                "id=" + id +
                ", nameOfTypeEnterprise='" + nameOfTypeEnterprise + '\'' +
                '}';
    }


    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprise) {
        this.enterprises = enterprise;
    }
}
