package com.springapp.mvc.services;

import com.springapp.mvc.db.dao.TypeOfEnterpriseDAO;
import com.springapp.mvc.db.entities.TypeOfEnterprise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeOfEnterpriseService {

    @Autowired
    TypeOfEnterpriseDAO typeOfEnterpriseDAO;

    public List<TypeOfEnterprise> findAll() throws Exception {

        return typeOfEnterpriseDAO.findAllTypes();
    }

    public TypeOfEnterprise findById(Integer id) {

        return typeOfEnterpriseDAO.findById(id);
    }

    public TypeOfEnterprise findByName(String name) {
        return typeOfEnterpriseDAO.findByName(name);
    }


    public boolean saveTypeOfEnterprise(TypeOfEnterprise typeOfEnterprise) {
        return typeOfEnterpriseDAO.saveTypeOfEnterprise(typeOfEnterprise);
    }
}
