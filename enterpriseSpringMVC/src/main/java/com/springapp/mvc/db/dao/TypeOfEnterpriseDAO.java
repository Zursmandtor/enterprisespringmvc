package com.springapp.mvc.db.dao;


import com.springapp.mvc.db.entities.Enterprise;
import com.springapp.mvc.db.entities.TypeOfEnterprise;
import com.springapp.mvc.utils.HibernateUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TypeOfEnterpriseDAO {

    /**
     * Database connection manager
     */
    private HibernateUtils hibernateUtils;

    @Autowired
    public TypeOfEnterpriseDAO(HibernateUtils hibernateUtils) {
        this.hibernateUtils = hibernateUtils;
    }

    public TypeOfEnterpriseDAO() {
    }

    public List<TypeOfEnterprise> findAllTypes() throws Exception{
        try {
            Session session = hibernateUtils.getSession();
            return session.createQuery("from TypeOfEnterprise ").list();
        } catch (HibernateException he) {
            he.printStackTrace();
            return new ArrayList();
        }
    }

    public TypeOfEnterprise findById(Integer id) {
        TypeOfEnterprise typeOfEnterprise = new TypeOfEnterprise();
        try {
            Session session = hibernateUtils.getSession();
            typeOfEnterprise = (TypeOfEnterprise) session.get(TypeOfEnterprise.class, new Integer(id));
            return typeOfEnterprise;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return typeOfEnterprise;
    }

    /**
     * Find enterprise by name.
     *
     * @param name department`s name
     * @return list of departments
     */
    public TypeOfEnterprise findByName(String name) {
        TypeOfEnterprise typeOfEnterprise = new TypeOfEnterprise();
        try {
            Session session = hibernateUtils.getSession();
            typeOfEnterprise = (TypeOfEnterprise) session.get(TypeOfEnterprise.class, new String(name));
            return typeOfEnterprise;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return typeOfEnterprise;
    }


    public boolean saveTypeOfEnterprise(TypeOfEnterprise typeOfEnterprise) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();

            session.beginTransaction();
            session.save(typeOfEnterprise);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return result == 1;
    }

}
