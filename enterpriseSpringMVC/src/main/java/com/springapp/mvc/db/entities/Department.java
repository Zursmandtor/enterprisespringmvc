package com.springapp.mvc.db.entities;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="disc", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("DepInh")
public class Department {

    private static final Logger log = Logger.getLogger(Department.class);

    /**
     * Department's name.
     */
    @Column(name="name")
    private String name;


    /**
     * Employees that work in this department.
     */
    @ManyToMany (fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name="department_employee",
            joinColumns = @JoinColumn(name="department_id"),
            inverseJoinColumns=@JoinColumn(name="employee_id"))
    private List<Employee> employeeList = new ArrayList<Employee>();


    /**
     * Department's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    protected Enterprise enterprise;


    /**
     * * Creates Department instance with specified name.
     *
     * @param id           department's id
     * @param name         department's name
     * @param employeeList list of department
     */
    public Department(Integer id, String name, List<Employee> employeeList) {
        this.id = id;
        this.name = name;
        this.employeeList = employeeList;
    }

    public Department(String name, List<Employee> employeeList) {
        this.id = 0;
        this.name = name;
        this.employeeList = employeeList;
    }

    /**
     * Creates Department instance with specified name.
     *
     * @param name department's name
     */
    public Department(String name) {
        this.name = name;
    }

    /**
     * Creates Department instance with "Undefined" name.
     */
    public Department() {
        this(0, "Undefined", null);

    }

    /**
     * Returns department's name.
     *
     * @return department's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the id to the department.
     *
     * @param id new department's id.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Sets the name to the department
     *
     * @param name new department's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * return employee`s list of the department
     *
     * @return employee`s list
     */
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    /**
     * Returns department's id
     *
     * @return department's id
     */
    public Integer getId() {
        return id;
    }


    /**
     * Sets employee`s list of the department
     *
     * @param employeeList all employees of department
     */
    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }


    public boolean addEmployee(Employee employee){
        return employeeList.add (employee);
    }

    public boolean deleteEmployee (Employee employee){
        return employeeList.remove(employee);
    }


    /**
     * String representation of a department
     *
     * @return string representation of a department
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(name);
        for (Employee employee : employeeList) {
            stringBuilder.append("\n");
            stringBuilder.append(employee.toString());
        }

        return stringBuilder.toString();
    }

    /**
     * Compares department and an object
     * (default IDEA generated code)
     *
     * @param o object to be compared with
     * @return true if o is an instance of Department class and has the same name and list of employees
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (!employeeList.equals(that.employeeList)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    /**
     * Default IDEA hashCode implementation
     *
     * @return hash code of an instance
     */
    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + employeeList.hashCode();
        return result;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }
}
