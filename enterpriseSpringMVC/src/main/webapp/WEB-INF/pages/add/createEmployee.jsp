<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<html>
<head>
    <title>Create Employee</title>
</head>
<body>
<form action="createEmployee" method="POST">
    <input name="idDepartment" value="${idDepartment}" hidden/>
    <label>
        <input name="employeeName" type="text">
        Enter name of employee
    </label>
    <br>
    <label>
        <input name="employeeAge" type="text">
        Enter age of employee
    </label>
    <br>
    <label>
    <input type="text" placeholder="Create new type" name="selectType"/>
        Create new type
    </label>
    <br>
    <c:if test="${fn:length(typesOfEmployee) != 0}">
    <select name="selectType">
        <c:forEach items="${typesOfEmployee}" var="typeOfEmployee">
            <option value="${typeOfEmployee.id}">${typeOfEmployee.nameOfTypeEmployee}</option>
        </c:forEach>
    </select>
    </c:if>
    <br>
    <input type="submit" value="Create"/>

</form>
</body>
</html>