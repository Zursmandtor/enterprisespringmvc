package com.springapp.mvc.db.dao;

import com.springapp.mvc.db.entities.TypeOfEmployee;
import com.springapp.mvc.utils.HibernateUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TypeOfEmployeeDAO {

    /**
     * Database connection manager
     */
    private HibernateUtils hibernateUtils;

    @Autowired
    public TypeOfEmployeeDAO(HibernateUtils hibernateUtils) {
        this.hibernateUtils = hibernateUtils;
    }

    public TypeOfEmployeeDAO() {
    }

    public List<TypeOfEmployee> findAllTypes() throws Exception{
        try {
            Session session = hibernateUtils.getSession();
            return session.createQuery("from TypeOfEmployee ").list();
        } catch (HibernateException he) {
            he.printStackTrace();
            return new ArrayList();
        }
    }

    public TypeOfEmployee findById(Integer id) {
        TypeOfEmployee typeOfEMployee = new TypeOfEmployee();
        try {
            Session session = hibernateUtils.getSession();
            typeOfEMployee = (TypeOfEmployee) session.get(TypeOfEmployee.class, new Integer(id));
            return typeOfEMployee;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return typeOfEMployee;
    }

    /**
     * Find enterprise by name.
     *
     * @param name department`s name
     * @return list of departments
     */
    public TypeOfEmployee findByName(String name) {
        TypeOfEmployee typeOfEMployee = new TypeOfEmployee();

        try {
            Session session = hibernateUtils.getSession();
            typeOfEMployee = (TypeOfEmployee) session.get(TypeOfEmployee.class, new String(name));
            return typeOfEMployee;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return typeOfEMployee;
    }


    public boolean saveTypeOfEmployee (TypeOfEmployee typeOfEMployee) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();

            session.beginTransaction();
            session.save(typeOfEMployee);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return result == 1;
    }

}
