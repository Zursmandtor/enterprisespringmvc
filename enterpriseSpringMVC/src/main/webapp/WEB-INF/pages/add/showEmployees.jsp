<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Employees</title>
</head>
<body>

<h1>Employees</h1>
<table border="1">
    <tr>
        <td>Name</td>
    </tr>

    <c:forEach items="${employees}" var="employee">
        <tr>
            <td>${employee.name}</td>
            <td>${employee.age}</td>
            <td>${employee.type}</td>
        </tr>
    </c:forEach>



</table>


<button onclick="window.location.href='/createEmployee?idDepartment=${idDepartment}'">Create Employee</button>


</body>
</html>