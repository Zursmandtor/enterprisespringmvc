package com.springapp.mvc.utils;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class HibernateUtils {

    @Autowired
    private SessionFactory sessionFactory;

    private Session session;

    public HibernateUtils(){
    }


    public Session getSession(){
        if (session == null || ! session.isOpen()){
            session=sessionFactory.openSession();
        }
        return session;
    }

    public void closeSession(){
        if(session != null || session.isOpen()){
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

}
