var table;
var addForm;

$(document).ready(function () {

    table = $('#enterprises');
    addForm = $('#addFrom');

    var width = table.find('table').width() + 25;
    $('body').css('width', width);

    addForm.slideUp(0);
});

function showAddFrom() {
    table.slideUp('slow');
    addForm.slideDown('slow');
}

function showTable() {
    addForm.slideUp('slow');
    table.slideDown('slow');
}
