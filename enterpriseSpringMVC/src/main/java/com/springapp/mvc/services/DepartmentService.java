package com.springapp.mvc.services;

import com.springapp.mvc.db.dao.DepartmentDAO;
import com.springapp.mvc.db.entities.Department;
import com.springapp.mvc.db.entities.Enterprise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    DepartmentDAO departmentDAO;

    public List<Department> findAll() {
        return departmentDAO.findAll();
    }

    public List<Department> findByEnterprise(Enterprise enterprise) {
        if (enterprise != null){
        return departmentDAO.findByEnterprise(enterprise);
        }
        return null;

    }

    public Department findById(Integer id) {

        return departmentDAO.findById(id);
    }

    public List<Department> findByName(String name) {

        return departmentDAO.findByName(name);
    }

    public boolean saveDepartment(Department department) {

        return departmentDAO.saveDepartment(department);
    }

    public boolean saveDepartmentToEnterprise(Enterprise enterprise, Department department) {
        return departmentDAO.saveDepartmentToEnterprise(enterprise, department);
    }

    public boolean removeDepartment(Department department) {

        return departmentDAO.removeDepartment(department);
    }

    public List getDepartmentsByEnterprise(Integer id) {

        return departmentDAO.getDepartmentsByEnterprise(id);

    }
}
