<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<html>
<head>
    <title>Create Enterprise</title>
</head>
<body>
<form action="createEnterprise" method="POST">
    <input type="text" placeholder="Enter name Enterprise" name="name"/>
    <br>
    <input type="text" placeholder="Create new type" name="selectType"/>
<br>

    <c:if test="${fn:length(typesOfEnterprise) != 0}">
        <select name="selectType">
        <c:forEach items="${typesOfEnterprise}" var="typeOfEnterprise">
        <option value="${typeOfEnterprise.id}">${typeOfEnterprise.nameOfTypeEnterprise}</option>
        </c:forEach>
        </select>
    </c:if>
    <br>

    <input type="submit"/>
</form>
</body>
</html>