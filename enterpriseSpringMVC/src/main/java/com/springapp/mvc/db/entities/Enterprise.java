package com.springapp.mvc.db.entities;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import com.springapp.mvc.db.entities.TypeOfEnterprise;



@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn (name="disc",discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("EntInh")
public class Enterprise {

    private static final Logger log = Logger.getLogger(Enterprise.class);

    /**
     * Enterprise's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    /**
     * Enterprise's name.
     */
    @Column(name="name")
    private String name;


    /**
     * Departments that are contained in this enterprise.
     */
    @OneToMany(fetch= FetchType.LAZY)
    @JoinColumn(name="enterprise_id")
    private List<Department> departmentList = new ArrayList();

    /**
     * Type of enterprise
     */
    @ManyToOne
    public TypeOfEnterprise typeOfEnterprise;

    /**
     * Creates Enterprise instance with specified name and departments list
     *
     * @param name enterprise's name
     */
    public Enterprise(Integer id, String name, List<Department> departmentList, TypeOfEnterprise typeOfEnterprise) {
        this.id = id;
        this.name = name;
        this.departmentList = departmentList;
        this.typeOfEnterprise = typeOfEnterprise;
    }

    public Enterprise(String name, List<Department> departmentList, TypeOfEnterprise typeOfEnterprise) {
        this.id = 0;
        this.name = name;
        this.departmentList = departmentList;
        this.typeOfEnterprise = typeOfEnterprise;
    }

    /**
     * Creates Enterprise instance with specified name
     *
     * @param name enterprise's name
     * @param typeOfEnterprise enterprise`s type
     */
    public Enterprise(String name, TypeOfEnterprise typeOfEnterprise) {
        this.name = name;
        this.typeOfEnterprise = typeOfEnterprise;
    }

    public Enterprise() {
    }

    /**
     * Sets the id to the enterprise
     *
     * @param id new enterprise's name
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Sets the name to the enterprise
     *
     * @param name new enterprise's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns enterprise's id
     *
     * @return enterprise's id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Returns enterprise's name
     *
     * @return enterprise's name
     */
    public String getName() {
        return name;
    }


    /**
     * Sets department`s list of the enterprise
     *
     * @param departmentList all departments of teh enterprise
     */
    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    /**
     * return department`s list of the enterprise
     *
     * @return department`s list
     */
    public List<Department> getDepartmentList() {
        if(departmentList==null){
            departmentList = new ArrayList<Department>();
        }
        return departmentList;
    }

    public boolean addDepartment(Department department) {
        if (departmentList.add(department)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteDepartment(Department department) {
        if (departmentList.remove(department)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return enterprise's type
     *
     * @return enterprise's type
     */
    public TypeOfEnterprise getTypeOfEnterprise() {
        return typeOfEnterprise;
    }


    /**
     * Set type of enterprise
     *
     * @param typeOfEnterprise enterprise`s type
     */
    public void setTypeOfEnterprise(TypeOfEnterprise typeOfEnterprise) {
        this.typeOfEnterprise = typeOfEnterprise;
    }

    /**
     * String representation of an enterprise
     *
     * @return string representation of an enterprise
     */
//    @Override
//    public String toString() {
//        StringBuilder stringBuilder = new StringBuilder(name.toUpperCase());
//        for (Department department : departmentList) {
//            stringBuilder.append("\n- - - - - - - - - - - - ");
//            stringBuilder.append(department.toString());
//        }
//        stringBuilder.append(TypeOfEnterprise);
//
//        return stringBuilder.toString();
//    }


    @Override
    public String toString() {
        return "Enterprise{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", departmentList=" + departmentList +
                ", TypeOfEnterprise='" + typeOfEnterprise + '\'' +
                '}';
    }

    /**
     * Compares department and an object
     * (default IDEA generated code)
     *
     * @param o object to be compared with
     * @return true if o is an instance of Department class and has the same name and list of employees
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Enterprise that = (Enterprise) o;

        if (!departmentList.equals(that.departmentList)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    /**
     * Default IDEA hashCode implementation
     *
     * @return hash code of an instance
     */
    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + departmentList.hashCode();
        return result;
    }


}
