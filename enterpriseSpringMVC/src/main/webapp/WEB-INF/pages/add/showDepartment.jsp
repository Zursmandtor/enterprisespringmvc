<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Departments</title>
</head>
<body>

<h1>Departments</h1>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Actions</td>

    </tr>

    <c:forEach items="${departments}" var="department">
        <tr>
            <td>${department.name}</td>
            <td>
                <button onclick="window.location.href='/createEmployeeAction?idDepartment=${department.id}'"> Show Employees </button>
            </td>
        </tr>
    </c:forEach>



</table>

<button onclick="window.location.href='/createDepartmentAction?idEnterprise=${idEnterprise}'">Create Department</button>


</body>
</html>