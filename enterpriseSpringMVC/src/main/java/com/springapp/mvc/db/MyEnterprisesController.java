package com.springapp.mvc.db;

import com.springapp.mvc.db.entities.*;
import com.springapp.mvc.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MyEnterprisesController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    EnterpriseService enterpriseService;

    @Autowired
    TypeOfEnterpriseService typeOfEnterpriseService;
    @Autowired
    TypeOfEmployeeService typeOfEmployeeService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showEnterprise(ModelMap modelMap) {
        modelMap.addAttribute("enterprises", enterpriseService.findAll());
        return "enterpriseTable";
    }

    @RequestMapping(value = "showDepartmentAction", method = RequestMethod.GET)
    public String showDepartmentsofEnterprise(ModelMap modelMap, @RequestParam Integer id) {
        modelMap.addAttribute("departments", departmentService.getDepartmentsByEnterprise(id));
        modelMap.addAttribute("idEnterprise", id);
        return "add/showDepartment";
    }

//    @RequestMapping(value = "createEmployeeAction", method = RequestMethod.GET)
//    public String showDepartmenAndfEnterprise(ModelMap modelMap, @RequestParam Integer idEnterprise) {
//        modelMap.addAttribute("departments", departmentService.getDepartmentsByEnterprise(idEnterprise));
//        modelMap.addAttribute("types", employeeService.getAllType());
//        return "add/createEmployee";
//    }


    @RequestMapping(value = "createEmployeeAction", method = RequestMethod.GET)
    public String showEmployeesofDepartment(ModelMap modelMap,
                                            @RequestParam Integer idDepartment) throws Exception{
        modelMap.addAttribute("idDepartment", idDepartment);
        modelMap.addAttribute("employees", employeeService.getEmployeessByDepartment(idDepartment));
        modelMap.addAttribute("typesOfEmployee", typeOfEmployeeService.findAll());
        return "add/showEmployees";
    }

    @RequestMapping(value = "createEnterpriseAction", method = RequestMethod.GET)
    public String createEnterprise(ModelMap model) throws Exception {

        model.addAttribute("typesOfEnterprise", typeOfEnterpriseService.findAll());
        return "add/createEnterprise";
    }

    @RequestMapping(value = "createDepartmentAction", method = RequestMethod.GET)
    public String createDepartment(ModelMap model,
                                   @RequestParam long idEnterprise) {
        model.addAttribute("id", idEnterprise);
        return "add/createDepartment";
    }

    @RequestMapping(value = "createEnterprise", method = RequestMethod.POST)
    public String addEnterprise(@RequestParam String name,
                                @RequestParam String selectType) {

        TypeOfEnterprise typeOfEnterprise = new TypeOfEnterprise(selectType);
        typeOfEnterpriseService.saveTypeOfEnterprise(typeOfEnterprise);
        Enterprise newEnterprise = new Enterprise(name, null, typeOfEnterprise);
        enterpriseService.saveEnterprise(newEnterprise);
        return "redirect:/";
    }

    @RequestMapping(value = "createDepartment", method = RequestMethod.POST)
    public String createDepartment(ModelMap model,
            @RequestParam String departmentName,
            @RequestParam int idEnterprise) {

        Enterprise enterprise = enterpriseService.findById(idEnterprise);
        Department department = new Department(departmentName, null);
        departmentService.saveDepartmentToEnterprise(enterprise, department);

        return showDepartmentsofEnterprise(model, idEnterprise);
    }

    @RequestMapping(value = "createEmployee", method = RequestMethod.POST)
    public String createEmployee(
            ModelMap model,
            @RequestParam String employeeName,
            @RequestParam int employeeAge,
            @RequestParam int idDepartment,
            @RequestParam String selectType) throws Exception {

        Department department = departmentService.findById(idDepartment);
        TypeOfEmployee typeOfEmployee = new TypeOfEmployee(selectType);
        typeOfEmployeeService.saveTypeOfEmployee(typeOfEmployee);
        Employee employee = new Employee(null, employeeName, employeeAge, typeOfEmployee);
        employeeService.saveEmployeeToDepartment(department, employee);
        return showEmployeesofDepartment(model,idDepartment);
    }


//
//    @RequestMapping(value = "/getDepartmentsAJAX", method = RequestMethod.GET)
//    public void getDepartmentsAJAX(@RequestParam String id, HttpServletResponse response) throws IOException {
//        int idInt = Integer.parseInt(id);
//        //int idInt = Integer.parseInt(id.substring(1));
//        PrintWriter writer = response.getWriter();
//        List<Department> departments = enterpriseService.findDepartmentByEnterprise(idInt);
//        writer.println("<table id=\"dep\" border=\"1\" cellspacing=\"0\"\n" +
//                "       cellpadding=\"5\">");
//        for (int i = 0; i < departments.size(); i++) {
//            writer.println("<tr>");
//            writer.println("<td>" + departments.get(i).getName() + "</td>");
//            writer.println("</tr>");
//        }
//        writer.println("</table>");
//    }
//


}
