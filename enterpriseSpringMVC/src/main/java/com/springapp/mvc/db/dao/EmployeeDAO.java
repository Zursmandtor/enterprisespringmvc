package com.springapp.mvc.db.dao;



import com.springapp.mvc.db.entities.Department;
import com.springapp.mvc.db.entities.Employee;
import com.springapp.mvc.db.entities.Enterprise;
import com.springapp.mvc.utils.HibernateUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class EmployeeDAO {

    /**
     * Create logger.
     */
    private static final Logger log = Logger.getLogger(EmployeeDAO.class);

    /**
     * Database connection manager
     */
    private HibernateUtils hibernateUtils;

    /**
     * Creates instance using connection manager
     *
     * @param hibernateUtils hibernate utils instance
     */
    @Autowired
    public EmployeeDAO(HibernateUtils hibernateUtils) {
        this.hibernateUtils = hibernateUtils;
    }

    public EmployeeDAO() {
    }

    /**
     * Find employee by id
     * @param id employee`s id
     * @return employee
     */
    public Employee findById(Long id){
        Employee employee = null;
        try{
        Session session  = hibernateUtils.getSession();
        employee = (Employee) session.get(Employee.class, new Long(id));
        log.debug("Employee founded");
        return employee;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employee;
    }

    /**
     * Find employee by name.
     * @param name employee`s name
     * @return list of employees
     */
    public List<Employee> findByName(String name){
        List<Employee> employees = null;
        try {
            Session session = hibernateUtils.getSession();
            employees = (List<Employee>) session.get(Department.class, new String(name));
            log.debug("Employee founded");
            return employees;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employees;
    }

    /**
     * Find employee by age.
     * @param age employee`s age
     * @return list of  employees
     */
    public List<Employee> findByAge(Integer age){
        List<Employee> employees = null;
        try {
            Session session = hibernateUtils.getSession();
            employees = (List<Employee>) session.get(Department.class, new Integer (age));
            log.debug("Employee founded");
            return employees;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employees;
    }

    /**
     * Find all employees in the department
     * @return list of employees
     */
    public Set<Employee> findAll(){
        Set<Employee> employeess = new HashSet();
        try {
            Session session = hibernateUtils.getSession();
            employeess = (Set<Employee>) session.createQuery ("from Employee")
                    .list();
            log.debug("Employee founded");
            return employeess;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employees wasn`t found", he);
        }
        return employeess;
    }

    /**
     * Save new employee.
      * @param employee new employee
     * @return new employee
     */
    public boolean saveEmployee(Employee employee) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
            log.debug("Employee was saved");
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Employee wasn`t saved", he);

        }
        return result == 1;
    }

    /**
     * Save new employee to the selection department and the selection enterprise
     * @param employee new employee
     * @param department selection department
     * @return new employee
     */
    public boolean saveEmployeeToDepartment(Department department, Employee employee) {
        int result = 0;

        try {
            Session session = hibernateUtils.getSession();

            session.beginTransaction();
            session.save(employee);
            session.save(department);
            department.getEmployeeList().add(employee);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Employee wasn`t saved", he);

        }
        return result == 1;
    }

    /**
     * Delete employee
     * @param employee deleting employee
     * @return deleting employee
     */
    public boolean removeEmployee (Employee employee){
        int result = 0;
        try{
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.delete(employee);
            session.getTransaction().commit();
        } catch (HibernateException he){
            he.printStackTrace();
            log.debug("Employee wasn`t deleted");
        }
        return result == 1;
    }

    /**
     * Ищет всех работников по отделу в предприятии
     * @param department отдел, в котором работают работники
     * @return список работников отдела
     */
    public List<Employee> findByDepartment(Department department) {
        List<Employee> employees = new ArrayList<Employee>();
        try {
            return department.getEmployeeList();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employees;
    }

    public List getEmployeessByEnterprise(Integer idDepartment) {
        try {
            Session session = hibernateUtils.getSession();
            Department department = (Department) session.load(Department.class, idDepartment); //get id enterprise
            return session.createQuery("from Employee as emp where emp.employeetList = (:department)")
                    .setEntity("department", department)
                    .list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return new ArrayList();
    }

    public List getAllType (){
        List allTypes;
        try {
            Session session = hibernateUtils.getSession();
            allTypes = (List) session.createQuery ("select distinct typeOfEmployee from Employee")
                    .list();
            log.debug("Employee founded");
            return allTypes;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employees wasn`t found", he);
        }
        return new ArrayList();

    }



}
