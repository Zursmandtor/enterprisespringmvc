package com.springapp.mvc.db.entities;


import javax.persistence.*;
import java.util.List;

@Entity
public class TypeOfEmployee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nameOfTypeEmployee;

    @OneToMany(mappedBy = "typeOfEmployee")
    private List<Employee> employees;

    public TypeOfEmployee(Long id, String nameOfTypeEmployee) {
        this.id = id;
        this.nameOfTypeEmployee = nameOfTypeEmployee;
    }

    public TypeOfEmployee(String nameOfTypeEmployee) {
        this.id = 0;
        this.nameOfTypeEmployee = nameOfTypeEmployee;
    }

    public TypeOfEmployee() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameOfTypeEmployee() {
        return nameOfTypeEmployee;
    }

    public void setNameOfTypeEmployee(String nameOfTypeEmployee) {
        this.nameOfTypeEmployee = nameOfTypeEmployee;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeOfEmployee that = (TypeOfEmployee) o;

        if (id != that.id) return false;
        if (employees != null ? !employees.equals(that.employees) : that.employees != null) return false;
        if (nameOfTypeEmployee != null ? !nameOfTypeEmployee.equals(that.nameOfTypeEmployee) : that.nameOfTypeEmployee != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nameOfTypeEmployee != null ? nameOfTypeEmployee.hashCode() : 0);
        result = 31 * result + (employees != null ? employees.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TypeOfEmployee{" +
                "id=" + id +
                ", nameOfTypeEmployee='" + nameOfTypeEmployee + '\'' +
                ", employees=" + employees +
                '}';
    }
}
