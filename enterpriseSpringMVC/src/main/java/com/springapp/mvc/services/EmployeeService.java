package com.springapp.mvc.services;

import com.springapp.mvc.db.dao.EmployeeDAO;
import com.springapp.mvc.db.entities.Department;
import com.springapp.mvc.db.entities.Employee;
import com.springapp.mvc.db.entities.Enterprise;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class EmployeeService {

    @Autowired
    EmployeeDAO employeeDAO;

    public Employee findById(Long id){

        return employeeDAO.findById(id);
    }


    public List<Employee> findByName(String name){

        return employeeDAO.findByName(name);
    }


    public List<Employee> findByAge(Integer age){

        return employeeDAO.findByAge(age);
    }


    public Set<Employee> findAll(){

        return employeeDAO.findAll();
    }


    public boolean saveEmployee(Employee employee) {

        return employeeDAO.saveEmployee(employee);
    }


    public boolean saveEmployeeToDepartment(Department department, Employee employee) {

        return employeeDAO.saveEmployeeToDepartment(department, employee);
    }


    public boolean removeEmployee (Employee employee){

        return employeeDAO.removeEmployee(employee);
    }


    public List<Employee> findByDepartment(Department department) {

        return employeeDAO.findByDepartment(department);
    }

    public List getEmployeessByDepartment(Integer idDepartment) {

        return employeeDAO.getEmployeessByEnterprise(idDepartment);

    }

    public List getAllType (){
        return employeeDAO.getAllType();

    }


}
