package com.springapp.mvc.services;


import com.springapp.mvc.db.dao.DepartmentDAO;
import com.springapp.mvc.db.dao.EnterpriseDAO;
import com.springapp.mvc.db.entities.Department;
import com.springapp.mvc.db.entities.Enterprise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EnterpriseService {

    @Autowired
    EnterpriseDAO enterpriseDAO;

    @Autowired
    DepartmentDAO departmentDAO;



    public List<Enterprise> findAll() {

            return enterpriseDAO.findAll();
    }


    public Enterprise findById(Integer id) {

        return enterpriseDAO.findById(id);
    }


    public Enterprise findByName(String name) {

        return enterpriseDAO.findByName(name);
    }


    public boolean saveEnterprise(Enterprise enterprise) {

        return enterpriseDAO.saveEnterprise(enterprise);
    }


    public boolean removeEnterprise(Enterprise enterprise) {

        return enterpriseDAO.removeEnterprise(enterprise);
    }

    public List<Department> findDepartmentByEnterprise(Integer id) {
        return departmentDAO.getDepartmentsByEnterprise(id);
    }


}
