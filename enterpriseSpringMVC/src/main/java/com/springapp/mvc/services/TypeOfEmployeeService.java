package com.springapp.mvc.services;


import com.springapp.mvc.db.dao.TypeOfEmployeeDAO;
import com.springapp.mvc.db.entities.TypeOfEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeOfEmployeeService {

    @Autowired
    TypeOfEmployeeDAO typeOfEmployeeDAO;

    public List<TypeOfEmployee> findAll() throws Exception {

        return typeOfEmployeeDAO.findAllTypes();
    }

    public TypeOfEmployee findById(Integer id) {

        return typeOfEmployeeDAO.findById(id);
    }

    public TypeOfEmployee findByName(String name) {
        return typeOfEmployeeDAO.findByName(name);
    }


    public boolean saveTypeOfEmployee (TypeOfEmployee typeOfEmployee) {
        return typeOfEmployeeDAO.saveTypeOfEmployee(typeOfEmployee);
    }
}
