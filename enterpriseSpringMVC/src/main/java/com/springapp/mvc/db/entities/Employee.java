package com.springapp.mvc.db.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Employee entity
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="disc", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("EmpInh")
public class Employee {


    /**
     * Employee's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Employee's name
     */

    private String name;

    /**
     * Employee's age
     */
    private int age;
    /**
     * Employee`s type
     */
    @ManyToOne
    public TypeOfEmployee typeOfEmployee;

    /**
     * Employees that work in this department.
     */
    @ManyToMany (mappedBy="employeeList")
    private List<Department> employeetList;

    public List<Department> getEmployeetList() {
        return employeetList;
    }

    public void setEmployeetList(List<Department> departmentList) {
        this.employeetList = departmentList;
    }

//    @ManyToOne(fetch= FetchType.LAZY)
//    protected TypeOfEmployee typeOfEMployee;



    public Employee(Integer id, String name, int age, TypeOfEmployee typeOfEmployee) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.typeOfEmployee = typeOfEmployee;

    }

    public Employee(String name, int age, TypeOfEmployee typeOfEmployee) {
        this.id = 0;
        this.name = name;
        this.age = age;
        this.typeOfEmployee = typeOfEmployee;

    }

    /**
     * Creates new employee with id = 0, 'Undefined' name and, age of 25, type 'Undefined'
     */
    public Employee() {

    }

    public TypeOfEmployee getTypeOfEmployee() {
        return typeOfEmployee;
    }

    public void setTypeOfEmployee(TypeOfEmployee typeOfEmployee) {
        this.typeOfEmployee = typeOfEmployee;
    }

    /**
     * Sets id to the employee
     *
     * @param id new employee's id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets name to the employee
     *
     * @param name new employee's name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Sets age to the employee
     *
     * @param age new employee's age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Returns employee`s id
     *
     * @return employee`s id
     */
    public int getId() {
        return id;
    }

    /**
     * Returns employee`s age
     *
     * @return employee`s age
     */
    public int getAge() {
        return age;
    }

    /**
     * Returns employee`s name
     *
     * @return employee`s name
     */
    public String getName() {
        return name;
    }




    /**
     * String representation of an employee
     *
     * @return string representation of an employee
     */
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    /**
     * Compares employee and an object
     * (default IDEA generated code)
     *
     * @param o object to be compared with
     * @return true if o is an instance of Employee class and has the same name and age
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (age != employee.age) return false;
        if (!name.equals(employee.name)) return false;

        return true;
    }

    /**
     * Default IDEA hashCode implementation
     *
     * @return hash code of an instance
     */
    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + age;
        return result;
    }



    public void setId(Integer id) {

        this.id = id;
    }
}
