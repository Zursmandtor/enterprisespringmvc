package com.springapp.mvc.db.dao;



import com.springapp.mvc.db.entities.Department;
import com.springapp.mvc.db.entities.Enterprise;
import com.springapp.mvc.utils.HibernateUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DepartmentDAO {

    /**
     * Create logger.
     */
    private static final Logger log = Logger.getLogger(DepartmentDAO.class);

    /**
     * Database connection manager
      */
    @Resource(name="hibernateUtils")
    private HibernateUtils hibernateUtils;

    public DepartmentDAO() {
    }

    public DepartmentDAO(HibernateUtils hibernateUtils) {
           this.hibernateUtils = hibernateUtils;
    }

    /**
     * Find all departments in the enterprise
     *
     * @return list of department
     */
    public List<Department> findAll() {
        List<Department> departments = new ArrayList();
        try {
            Session session = hibernateUtils.getSession();
            departments = (List<Department>) session.createQuery("from Department")
                    .list();
            return departments;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return departments;
    }

    public List<Department> findByEnterprise(Enterprise enterprise) {
        List<Department> departments = new ArrayList();
        try {
            Session session = hibernateUtils.getSession();
            departments = (List<Department>) session.createQuery("from Department as dep where " +
                    "(:enterprise) in elements(dep.employeeList) ")
                    .setEntity("enterprise", enterprise)
                    .list();
            return departments;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return departments;
    }

    public List getDepartmentsByEnterprise(Integer id) {
        try {
            Session session = hibernateUtils.getSession();
            Enterprise enterprise = (Enterprise) session.load(Enterprise.class, id); //get id enterprise
            return session.createQuery("from Department as dep where dep.enterprise = (:enterprise)")
                    .setEntity("enterprise", enterprise)
                    .list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return new ArrayList();
    }

    /**
     * Find department by id
     *
     * @param id department`s id
     * @return department
     */
    public Department findById(Integer id) {
        Department department = null;
        try {
            Session session = hibernateUtils.getSession();
            department = (Department) session.get(Department.class, new Integer(id));

            return department;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return department;
    }

    /**
     * Find department by name.
     *
     * @param name department`s name
     * @return list of departments
     */
    public List<Department> findByName(String name) {
        List<Department> departments = null;
        try {
            Session session = hibernateUtils.getSession();
            departments = (List<Department>) session.get(Department.class, new String(name));
            return departments;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return departments;
    }

    /**
     * Save new department
     *
     * @param department new department
     * @return new department
     */
    public boolean saveDepartment(Department department) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();

            session.beginTransaction();
            session.save(department);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t saved", he);

        }
        return result == 1;
    }


    /**
     * Save new department
     *
     * @param department new department
     * @return new department
     */
    public boolean saveDepartmentToEnterprise(Enterprise enterprise, Department department) {
        try {
            enterprise.getDepartmentList().add(department);
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.saveOrUpdate(enterprise);
            session.save(department);
            session.getTransaction().commit();

            return true;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t saved", he);
            return false;
        }
    }


    /**
     * Delete department
     *
     * @param department deleting department
     * @return deleting department
     */
    public boolean removeDepartment(Department department) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.delete(department);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.debug("Department was deleted");
        }
        return result == 1;
    }


}


