<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Enterprise Table</title>
    <link href="../styles/style.css" type="text/css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-2.1.0.js"></script>
    <%--<script src="../styles/script.js"></script>--%>
</head>
<body>

<h1>${message}</h1>


<div id="container">

    <table border="1" cellspacing="1" cellpadding="5">

        <tr>
            <td>Enterpise's name</td>
            <td>Actions</td>
        </tr>


        <c:forEach items="${enterprises}" var="enterprise">
            <tr>
                <td>${enterprise.name}</td>

                <td>
                    <button onclick="window.location.href='/showDepartmentAction?id=${enterprise.id}'"> Show Departments </button>
                </td>

            </tr>
        </c:forEach>
    </table>
    <br>
    <br>

    <button onclick="window.location.href='/createEnterpriseAction'">Create Enterprise</button>



</div>


</body>
</html>
<%--<script>--%>

    <%--var id = 0;--%>
    <%--function showDepartment(enterpriseID) {--%>
        <%--$("#departments_" + id).html('')--%>
        <%--$.get("/getDepartmentsAJAX?id=" + enterpriseID, function (data) {--%>
            <%--$("#departments_" + enterpriseID).html(data)--%>

        <%--});--%>
        <%--id = enterpriseID--%>
    <%--}--%>


<%--</script>--%>

<%--<script>--%>
    <%--var table;--%>
    <%--var addForm;--%>

    <%--$(document).ready(function () {--%>

        <%--table = $('#enterprises');--%>
        <%--addForm = $('#addFrom');--%>

        <%--var width = table.find('table').width() + 25;--%>
        <%--$('body').css('width', width);--%>

        <%--addForm.slideUp(0);--%>
    <%--});--%>

    <%--function showAddFrom() {--%>
        <%--table.slideUp('slow');--%>
        <%--addForm.slideDown('slow');--%>
    <%--}--%>

    <%--function showTable() {--%>
        <%--addForm.slideUp('slow');--%>
        <%--table.slideDown('slow');--%>
    <%--}--%>
<%--</script>--%>


